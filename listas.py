edad = 4
nombre = 'Andrea'

elementos = [1, 4, 'python', True, 3, 4.6]
print(elementos)
numero = []
numeros = [3, 4, 6, 3, 1, 9, 8, 7]
print(numeros)

# Obtiene la longitud de la lista
longitud = len(numeros)
print(longitud)
# Acceso a través de los índices
print(numeros[0])
print(numeros[-1])
print(numeros[3:6])
# Agregar un elemento a una lista
numeros.append(100)
print(numeros)
# Inserta un elemento en la lista
# Si se sale de la longitud maxima, inserta el elemento
# al final de la lista
numeros.insert(20, 200)
print(numeros)
# Reemplaza un elemento de la lista
numeros[0] = 150
print(numeros)
# Reemplaza una sección de la lista
numeros[3:6] = [20, 30, 40]
print(numeros)
# Eliminar una parte de la lista
numeros[3:6] = []
print(numeros)
# Elimina un elemento de una lista
numeros.remove(100)
print(numeros)
# Elimina el ultimo elemento de la lista
numeros.pop()
print(numeros)
# Invierte la lista
numeros.reverse()
print(numeros)
# Ordenar la lista ascendentemente
numeros.sort()
print(numeros)
# Ordenar la lista descendentemente
numeros.sort(reverse=True)

# Matriz = lista de listas
triqui = [
    ['X', 'O', 'X'],
    ['O', 'X', ' '],
    [' ', ' ', 'X']
]
print(triqui)

notas = []
nota1 = input('Digite la nota 1: ')
nota2 = input('Digite la nota 2: ')
nota3 = input('Digite la nota 3: ')

notas.append(float(nota1))
notas.append(float(nota2))
notas.append(float(nota3))

print(notas)
