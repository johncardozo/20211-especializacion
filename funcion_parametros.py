def mensaje():
    print('USTA')


def imprimir_nombre(nombre):
    print(f'El nombre es {nombre}')


def suma(num1, num2):
    resultado = num1 + num2
    return resultado


def suma_resta(num1, num2):
    s = num1 + num2
    r = num1 - num2
    return s, r


# Funcion simple
mensaje()
# Funcion con parametros
imprimir_nombre('John')
imprimir_nombre('Paul')
imprimir_nombre('George')
imprimir_nombre('Ringo')

# Funcion con parametros con valor retorno
r1 = suma(3, 4)
r2 = suma(4, 2)
r3 = r1 * r2
print(f'{r1} * {r2} = {r3}')

# Funcion con retorno multiple
a, b = suma_resta(8, 3)
print(f'{a} y {b}')
