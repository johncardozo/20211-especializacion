# Importa la librería de JSON
import json

# Definición de datos en un diccionario
datos = {
    "username": "johncardozo",
    "nombres": "john",
    "apellidos": "cardozo",
    "edad": 25,
    "activo": True
}

# 1. Abrir el archivo para guardar la información (w: write)
archivo = open('data/cuenta.json', 'w')

# 2. Guarda el diccionario en el archivo
json.dump(datos, archivo)

# 3. Cierra el archivo
archivo.close()
