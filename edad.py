# Solicitar la edad al usuario
edad_usuario = input('Digite la edad: ')

# Verifica si la edad digitada es numérica
if edad_usuario.isdecimal():
    # Convierte el valor digitado en entero
    edad = int(edad_usuario)
    # Edad entre 0 y 10 -> Niño
    if edad >= 0 and edad <= 10:
        print('Es un niño')
    # Edad entre 11 y 20 -> Adolescente
    elif edad >= 11 and edad <= 20:
        print('Es un adolescente')
    # Edad entre 21 y 60 -> Adulto
    elif edad >= 21 and edad <= 60:
        print('Es un adulto')
    # Edad es mayor 60 -> tercera edad
    elif edad > 60:
        print('Es de la tercera edad')
else:
    print('Edad no válida!')
