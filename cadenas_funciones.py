# Declaración de la variable
frase = 'Universidad Santo Tomás'
print(frase)

# Longitud de una cadena
longitud = len(frase)
print(longitud)

# Verifica si una variable termina con
# un valor específico
termina = frase.endswith('más')
print(termina)

# Verifica si una variable termina con
# un valor específico
dato = input('Digite la cadena de búsqueda: ')
inicia = frase.startswith(dato)
print(inicia)

# Obtiene el índice de una subcadena en otra
posicion = frase.find('ooo')
print(posicion)

edad = 28
estudiante = 'Andrea'
# Concatenar strings
mensaje1 = 'El estudiante ' + estudiante + ' tiene ' + str(edad) + ' años'
print(mensaje1)
# Formato de cadena
mensaje2 = f'El estudiante {estudiante} tiene {edad} años'
print(mensaje2)

# Titulo
titulo = 'Lo que el Viento se llevó - 1939'
print(titulo.title())

# Cuenta la cantidad de ocurrencias de un string en otro
ocurrencias = titulo.count('e')
print(ocurrencias)

# Cambio a mayusculas/minusculas
print(titulo.lower())
print(titulo.upper())

# Analisis de cadenas
password = '1XY$Z'
print(password.isalnum())
print(password.isalpha())
print(password.isdecimal())
print(password.islower())
print(password.isupper())
