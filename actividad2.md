# Aplicación de Favoritos

Hacer una aplicación en Python que muestr el siguiente menú:

1. Agregar favorito
   - Titulo
   - URL
   - Comentario
2. Eliminar favorito
   - Titulo
3. Modificar favorito
   - Titulo
   - Nuevo titulo
   - Nuevo URL
   - Nuevo comentario
4. Ver la lista de favoritos
5. Salir

El programa debe guardar todos los datos en un archivo JSON.
