# Solicita datos por consola
# SIEMPRE obtiene un string
nombre = input('Digite su nombre: ')
edad = input('Digite su edad: ')
# Muestra el valor de las variables
print(nombre)
print(edad)
# Muestra el tipo de las variables
print(type(nombre))
print(type(edad))

##############

numero1 = input('Digite el numero 1: ')
numero2 = input('Digite el numero 2: ')

# Funciones de transformación: int, float, str, bool
#suma = numero1 + numero2
suma = int(numero1) + int(numero2)

# Muestra la variable por consola
print(suma)
