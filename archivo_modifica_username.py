import json
from datetime import datetime

# Ruta del archivo JSON
ruta = 'data/cuenta.json'

# Pide el username al usuario
nuevo_username = input('Digite el nuevo username: ')

# Lee del archivo
archivo = open(ruta, 'r')
datos = json.load(archivo)
archivo.close()

# Modifica el diccionario
datos["username"] = nuevo_username
datos["fecha_actualizacion"] = str(datetime.now())

# Escribir en el archivo
archivo = open(ruta, 'w')
json.dump(datos, archivo)
archivo.close()
