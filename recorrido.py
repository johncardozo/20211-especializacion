numeros = [3, 6, 8, 5, 4, 1, 3, 5, 7, 6, 5]

# Recorre una lista
for numero in numeros:
    # Verificacion
    if numero % 2 == 0:
        print(f'{numero} es par')
    else:
        print(f'{numero} es impar')
