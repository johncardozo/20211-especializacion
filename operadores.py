# Operador de asignación =
a = 4
b = 5

# Operador de suma
suma = a + b
print(suma)
# Operador de resta
resta = b - a
print(resta)
# Operador de multiplicacion
multi = a * b
print(multi)
# Operador de division
division = a / b
print(division)
# Operador de modulo (residuo)
modulo = 20 % 3
print(modulo)
# Operador de potencia
potencia = a ** 2
print(potencia)
