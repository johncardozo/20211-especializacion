precio = 45

# Preguntar por una sola cosa
# if
if precio > 50:
    print('Esta caro!')

# Preguntar dos opciones
# if/else
if precio > 100:
    print('esta carisimo')
else:
    print('se puede comprar')

# Preguntar por multiples opciones
numero = int(input('Digite el numero: '))
if numero == 0:
    print('Zero')
elif numero == 1:
    print('One')
elif numero == 2:
    print('Two')
elif numero == 3:
    print('Three')
else:
    print('No es ninguno de los valores')


valor = 10
if valor > 5:
    x = 5
    y = 6
    valor = valor + 3
    resultado = x + y + valor
    print(valor)
    if valor > 10:
        print('mayor a 10')
else:
    a = 5
    b = a + 4
    res = a * b
    print(res)
