# Importa la libreria
import json

# 1. Abrir el archivo (r: read)
archivo = open('data/cuenta.json', 'r')

# 2. Lee la información y la guarda en el diccionario
contenido = json.load(archivo)

print(contenido)

# 3. Cierra el archivo
archivo.close()