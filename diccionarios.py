# Declaración de un diccionario vacio
persona = {}

# Declaración de un diccionario con información
curso = {
    "materia": "Python & AWS",
    "creditos": 4,
    "homologable": False,
    "palabras_clave": ['python', 'aws', 'nube'],
    "profesor": {
        "nombre": "John Cardozo",
        "edad": 25
    },
    "estudiantes": [
        {"nombre": "John", "edad": 10},
        {"nombre": "George", "edad": 12},
        {"nombre": "Paul", "edad": 11},
    ]
}
# Imprime todo el diccionario
# print(curso)

# Acceso por las llaves
print(curso["materia"])
print(curso["homologable"])
print(curso["profesor"])

# Acceso por las llaves y subllaves
print(f'El nombre del profesor es {curso["profesor"]["nombre"]} ')

print(curso)
# Modifica el valor de una propiedad a través de su llave.
# PERO si la llave no existe agrega la propiedad
curso["materia"] = 'Diseño de redes'
print(curso)

# Eliminar una propiedad por su llave
del curso["creditos"]
print(curso)

# Recorre una lista dentro de un diccionario
for estudiante in curso["estudiantes"]:
    print(estudiante["nombre"])

lista_estudiantes = curso["estudiantes"]
for estudiante in lista_estudiantes:
    print(estudiante["nombre"])
