nombre = 'Catalina'
apellidos = "Cardozo"

print(nombre)
print(apellidos)

# I don't know
#mensaje1 = 'I don\'t know'
mensaje1 = "I don't know"
print(mensaje1)

# \n = bajar a la la siguiente línea
mensaje2 = 'Primer renglón\nSegundo renglón'
print(mensaje2)

# r = raw (datos sin interpretar)
ruta = r'c:\datos\nomina.xls'
print(ruta)

# Acceso a partes del string
universidad = "Universidad Santo Tomás"
print(universidad)
# Un caracter
letra1 = universidad[1]
print(letra1)
# Substring
sub1 = universidad[2:6]
print(sub1)
# Indices negativos
sub2 = universidad[-2]
print(sub2)
# Obviar parametros
sub3 = universidad[:4]  # desde el principio
print(sub3)
sub4 = universidad[4:]  # hasta el final
print(sub4)
