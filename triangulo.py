# Pide los datos al usuario
base = input('Digite la base: ')
altura = input('Digite la altura: ')

# Hace el cálculo
area = (float(base) * float(altura)) / 2

# Muestra el resultado
print(f'El área del triángulo es {area}')
