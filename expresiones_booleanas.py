edad = 19

print(edad > 18)  # Mayor
print(edad < 18)  # Menor
print(edad >= 18)  # Mayor o igual
print(edad <= 18)  # Menor o igual
print(edad == 18)  # Igual

# AND: Todos verdaderos
# OR: Al menos uno verdadero
precio = 300
print(precio > 200 or precio > 250)
print(precio >= 200 and precio < 300)

print(not precio > 100)
