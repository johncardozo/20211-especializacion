CIRCULO = 1
CUADRADO = 2
TRIANGULO = 3


def menu():
    # Muestra el menu
    print('\nCALCULADORA DE FIGURAS')
    print('1. Area de circulo')
    print('2. Area de cuadrado')
    print('3. Area de triangulo')
    print('4. Salir')
    # Pide los datos al usuario
    opcion = input('Digite la opcion: ')
    # Retorna la opcion digitada
    return int(opcion)


def pedir_datos_circulo():
    '''
    Solicita los datos de calculo de el area del circulo
    '''
    radio = input('Digite el radio del circulo: ')
    return float(radio)


def pedir_datos_cuadrado():
    '''
    Solicita los datos de calculo del area del cuadrado
    '''
    lado = input('Digite el lado del cuadrado: ')
    return float(lado)


def muestra_area(figura, area):
    '''
    Muestra el resultado del area en la terminal
    '''
    # Verifica el tipo de figura a mostrar
    figura_letrero = ''
    if figura == CIRCULO:
        figura_letrero = 'circle'
    if figura == CUADRADO:
        figura_letrero = 'square'
    if figura == TRIANGULO:
        figura_letrero = 'triangle'

    print(f'The area of the {figura_letrero} is {area}')


def pedir_datos_triangulo():
    '''
    Solicita los datos de calculo del area del triangulo
    '''
    base = input('Digite la base del triangulo: ')
    altura = input('Digite la altura del triangulo: ')
    # Retorna ambos datos
    return float(base), float(altura)
