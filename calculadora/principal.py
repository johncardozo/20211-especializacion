from figuras import *
from interfaz import (
    menu,
    pedir_datos_circulo,
    pedir_datos_cuadrado,
    pedir_datos_triangulo,
    muestra_area
)

# Constantes
CIRCULO = 1
CUADRADO = 2
TRIANGULO = 3
SALIR = 4

# Ciclo general del programa
opcion = 0
while opcion != SALIR:
    # Muestra le menu al usuario
    opcion = menu()

    # Analiza la opcion digitada
    if opcion == CIRCULO:
        # Pide los datos al usuario
        radio = pedir_datos_circulo()
        # Calcula el area del circulo
        area = area_circulo(radio)
        # Muestra el resultado
        muestra_area(CIRCULO, area)
    elif opcion == CUADRADO:
        # Pide los datos al usuario
        lado = pedir_datos_cuadrado()
        # Calcula el area del cuadrado
        area = area_cuadrado(lado)
        # Muestra el resultado
        muestra_area(CUADRADO, area)
    elif opcion == TRIANGULO:
        # Pide los datos al usuario
        base, altura = pedir_datos_triangulo()
        # Calcula el area del triangulo
        area = area_triangulo(base, altura)
        # Muestra el resultado
        muestra_area(TRIANGULO, area)
    elif opcion == SALIR:
        print('\nGracias por usar la calculadora!!!')
    else:
        print('Opcion erronea!')
