import math


def area_cuadrado(lado):
    '''
    Calcula el area de un cuadrado dado un lado.
    '''
    return lado * lado


def area_circulo(radio):
    '''
    Calcula el area de un circulo dado su radio.
    '''
    area = math.pi * radio * radio
    return area


def area_triangulo(base, altura):
    '''
    Calcula el area de un triangulo dada su base y su altura.
    '''
    area = (base * altura) / 2
    return area
