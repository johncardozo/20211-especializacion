# Inicialización
contador = 0
# Ejecuta las instrucciones de 6 a 9
# mientras que contador sea menor o igual a 4
while contador <= 4:
    # Imprime el contador
    print(f'El contador = {contador}')
    # Instrucción de rompimiento
    contador = contador + 1

valor = ''
while valor != 'salir':
    valor = input('digite el valor: ')
    print(f'Usted digitó {valor}')

print('Gracias por digitar los valores!')

numero = 5
while numero % 2 == 1:
    n = input('Digite el numero (impar): ')
    numero = int(n)
    print(f'Usted digitó {n}')

print('Gracias por digitar los números!')
